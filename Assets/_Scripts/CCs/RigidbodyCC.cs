﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidbodyCC : MonoBehaviour
{
	[SerializeField] private bool isAllowedToMove = false;

	[Header("Movement")]
	[SerializeField] private float acceleration = 10f;
	[SerializeField] private float friction = 0.2f;
	[SerializeField] private float jumpHeight = 5f;
	[SerializeField] private float jumpForce = 5f;
	[SerializeField] private float maxSpeed = 4f;

	[Header("Cast Options")]
	[SerializeField] private bool isSphereCast = true;
	[SerializeField] private LayerMask castLayer;

	[Header("SphereCast")]
	[SerializeField] private float scyOffset = 1f;
	[SerializeField] private float sphereCastRadius = 0.5f;

	[Header("RayCast")]
	[SerializeField] private float rayCastradius = 0.5f;

	private Rigidbody rb;
	private Animator anim;
	private Vector3[] raycastOffsets = new Vector3[4];
	private Vector3 moveDir;
	private Vector3 velocity;
	[SerializeField] private bool isGrounded;

	#region Monobehaviour
	void Start()
	{
		rb = GetComponent<Rigidbody>();
		anim = GetComponentInChildren<Animator>();
		raycastOffsets[0] = Vector3.forward * rayCastradius;
		raycastOffsets[1] = Vector3.right * rayCastradius;
		raycastOffsets[2] = Vector3.back * rayCastradius;
		raycastOffsets[3] = Vector3.left * rayCastradius;
	}
	void Update()
	{
		if (isAllowedToMove)
			MovementInput();

		GroundCheck();
		GetOtherInput();
	}
	void FixedUpdate()
	{
		Move();
	}
	#endregion

	#region Updated Input Calculation
	void MovementInput()
	{
		// get inputs and set a direction vector
		float horizontal = Input.GetAxis("Horizontal");
		float vertical = Input.GetButton("Fire3") ? Input.GetAxis("Vertical") : Input.GetAxis("Vertical") * 0.4f;

		moveDir = new Vector3(horizontal, 0f, vertical);

		anim.SetFloat("Horizontal", moveDir.x);
		anim.SetFloat("Vertical", moveDir.z);
	}
	void GroundCheck()
	{
		// SphereCast Check
		if (isSphereCast)
		{
			if (Physics.SphereCast(transform.position + Vector3.up * scyOffset, sphereCastRadius, Vector3.down, out RaycastHit hit, scyOffset + 0.05f - sphereCastRadius, castLayer))
			{
				if (!isGrounded)
				{
					rb.velocity = Vector3.zero;
					isGrounded = true;
				}
			}
			else
				isGrounded = false;
		}
		else        // RayCast Check
		{
			// cast from the center
			isGrounded = isGrounded || Physics.Raycast(
				new Ray(transform.position + Vector3.up * scyOffset, Vector3.down), scyOffset + 0.01f, castLayer);
			Debug.DrawRay(transform.position + Vector3.up * scyOffset, Vector3.down, Color.green);

			// cast from the border
			for (int i = 0; i < raycastOffsets.Length; i++)
			{
				isGrounded = isGrounded || Physics.Raycast(
					new Ray(transform.position + Vector3.up * scyOffset + raycastOffsets[i], Vector3.down), scyOffset + 0.01f, castLayer);
				Debug.DrawRay(transform.position + Vector3.up * scyOffset + raycastOffsets[i], Vector3.down, Color.red);
			}
		}
	}
	void GetOtherInput()
	{
		// Jump
		if (isGrounded && Input.GetButtonDown("Jump"))
			rb.AddForce(Vector3.up * jumpHeight + velocity * jumpForce, ForceMode.Impulse);
	}
	#endregion

	#region Fixed Update
	void Move()
	{
		// only move if we are grounded
		if (isGrounded)
		{
			velocity += moveDir * acceleration;
			velocity = Vector3.ClampMagnitude(velocity, maxSpeed);


			Debug.Log(velocity);
			rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
			velocity -= velocity * friction;
		}
	}
	#endregion
}