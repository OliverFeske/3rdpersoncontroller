﻿using UnityEngine;

public class CustomCC : MonoBehaviour
{
	[SerializeField] private bool isAllowedToMove = false;

	[Header("Movement")]
	[SerializeField] private float maxSpeed = 5f;
	[SerializeField] private float acceleration = 10f;
	[SerializeField] private float friction = 5f;
	[SerializeField] private float jumpForce = 5f;
	[SerializeField] private float drag = 2f;

	[Header("Collision Check")]
	[SerializeField] private float collisionCheckRadius = 5f;

	[Header("World Physics")]
	[SerializeField] private Vector3 gravity = new Vector3(0f, -9.81f, 0f);
	private Vector3 velocity;
	private Collider[] colliders = new Collider[12];

	#region Monobehaviour
	void Update()
	{
		if (isAllowedToMove)
			MovementInput();
	}
	#endregion

	#region Updated Input Calculation
	void MovementInput()
	{
		float vertical = Input.GetAxis("Vertical");
		float horizontal = Input.GetAxis("Horizontal");

		Vector3 moveDir = new Vector3(horizontal, 0f, vertical).normalized;

		Move(moveDir);
	}
	#endregion

	#region Apply Inputs
	void Move(Vector3 moveDir)
	{

		velocity += moveDir * acceleration;
		velocity = Vector3.ClampMagnitude(velocity, maxSpeed);

		//if (cc.isGrounded)
		//	if (Input.GetButtonDown("Jump"))
		//		velocity.y = jumpForce;
		//	else
		//		velocity += gravity * Time.deltaTime;

		transform.position += velocity * Time.deltaTime;
		Collision();
		velocity -= velocity * friction;
		velocity += gravity * Time.deltaTime;
		velocity -= gravity * drag * Time.deltaTime;
	}
	#endregion

	#region Physics Calculation
	void Collision()
	{
		int count = Physics.OverlapSphereNonAlloc(transform.position, collisionCheckRadius, colliders);
		Collider thisCol = GetComponent<Collider>();
		for (int i = 0; i < count; i++)
		{
			Collider col = colliders[i];

			if (col == thisCol)
				continue;

			bool isColliding = Physics.ComputePenetration(
				thisCol, 
				transform.position, 
				transform.rotation, 
				col, 
				col.transform.position, 
				col.transform.rotation, 
				out Vector3 direction, 
				out float distance);

			if (isColliding)
				transform.position += direction * distance;
		}
	}
	#endregion
}
