﻿using UnityEngine;

public class OpenDevConsole : MonoBehaviour
{
	private bool isVisible = true;

	void Start()
	{
		Debug.developerConsoleVisible = isVisible;
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Insert))
		{
			Debug.developerConsoleVisible = !isVisible;
		}
	}
}
